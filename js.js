function produto (nome, tipo, preco, descricao){
    this.nome = nome;
    this.tipo = tipo;
    this.preco = preco;
    this.descricao = descricao;
}
// lista

// chamar a função
const meuProduto = new produto("BMW", "carro", "130.000", "ideal para role");

//pegar id da lista
const lista = document.getElementById('lista');
//criar elemento para cada campo
const itemPro = document.createElement('li')
const itemTip = document.createElement('li')
const itemPre = document.createElement('li')
const itemDes = document.createElement('li')
//atribuir valores para cada elemento
itemPro.innerHTML = "Produto: "+" " +  meuProduto.nome;
itemTip.innerHTML  = "Tipo: "+" " + meuProduto.tipo;
itemPre.innerHTML = "Preço: "+" " + meuProduto.preco;
itemDes.innerHTML  = "Descrição: "+" " + meuProduto.descricao;
// adicionando cada elemento na lista
lista.appendChild(itemPro);
lista.appendChild(itemTip);
lista.appendChild(itemPre);
lista.appendChild(itemDes);
